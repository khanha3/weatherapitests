﻿using System;
using NUnit.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using WeatherAPITest.WeatherData;
using WeatherAPITest.WeatherData.DataHandling;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace WeatherAPITest.Tests
{
    [TestFixture]
    public class CurrentWeatherTests
    {
        private CurrentWeatherService currentWeather = new CurrentWeatherService("London");

        [Test]
        public void ResponseConatainsName()
        {
            Assert.IsTrue(currentWeather.json_weather.ContainsKey("name"));
        }
        [Test]
        public void ResponseSuccesCheck()
        {
            Assert.AreEqual(currentWeather.weatherDTO.CurrentWeatherRootobject.name, "London");
        }

        [Test]
        public void CheckCoordinates()
        {
            Assert.AreEqual(51.51 ,currentWeather.weatherDTO.CurrentWeatherRootobject.coord.lat, 2 );
            Assert.AreEqual(-0.13, currentWeather.weatherDTO.CurrentWeatherRootobject.coord.lon, 2);
        }

        [Test]
        public void CheckTimeZone()
        {
            Assert.AreEqual(0, currentWeather.weatherDTO.CurrentWeatherRootobject.timezone);
        }


    }
}
