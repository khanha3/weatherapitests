﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using  WeatherAPITest.WeatherData.DataHandling;
using WeatherAPITest.WeatherData.HTTPManger;

namespace WeatherAPITest.WeatherData
{
    class CurrentWeatherService
    {
        public WeatherCallManager weathercaller = new WeatherCallManager();

        public WeatherDTO weatherDTO = new WeatherDTO();

        public String currentWeather;

        public JObject json_weather;

        public CurrentWeatherService(string city)
        {
            currentWeather = weathercaller.GetLatestWeather(city);
            weatherDTO.DeserializeLatestRates(currentWeather);
            json_weather = JsonConvert.DeserializeObject<JObject>(currentWeather);

        }
        public CurrentWeatherService(string city, string state)
        {
            currentWeather = weathercaller.GetLatestWeather(city , state);
            weatherDTO.DeserializeLatestRates(currentWeather);
            json_weather = JsonConvert.DeserializeObject<JObject>(currentWeather);

        }

        public CurrentWeatherService(double lat, double lon)
        {
            currentWeather = weathercaller.GetLatestWeatherGeo(lat, lon);
            weatherDTO.DeserializeLatestRates(currentWeather);
            json_weather = JsonConvert.DeserializeObject<JObject>(currentWeather);

        }


    }
}
