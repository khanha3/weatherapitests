﻿using WeatherAPITest.WeatherData;
using Newtonsoft.Json;

namespace WeatherAPITest.WeatherData.DataHandling
{
    class WeatherDTO
    {
        // The class is the model of data.
        public Rootobject CurrentWeatherRootobject { get; set; }

        // Method that creates the above object following passing in the response from the API
        public void DeserializeLatestRates(string currentWeatherResponse)
        {
            CurrentWeatherRootobject = JsonConvert.DeserializeObject<Rootobject>(currentWeatherResponse);
        }

    }
}
