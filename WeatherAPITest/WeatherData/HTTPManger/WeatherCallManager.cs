﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace WeatherAPITest.WeatherData.HTTPManger
{
    public class WeatherCallManager
    {
        private readonly IRestClient client;

        public WeatherCallManager()
        {
            client = new RestClient(AppConfigReader.BaseUrl);
            //client = new RestClient("http://api.openweathermap.org/data/2.5/weather?q=London&appid=976677dd80f2d362dfec01298917664b");
        }

        public string GetLatestWeather()
        {
            var request = new RestRequest("/weather").AddQueryParameter("q","London").AddQueryParameter("appid", AppConfigReader.ApiKey);
            var response = client.Execute(request, Method.GET);
            return response.Content;
        }

        public string GetLatestWeather(string city)
        {
            var request = new RestRequest("/weather" ).AddQueryParameter("q",city).AddQueryParameter("appid", AppConfigReader.ApiKey);
            var response = client.Execute(request, Method.GET);
            return response.Content;
        }

        public string GetLatestWeather(string city, string state)
        {
            var request = new RestRequest("/weather").AddQueryParameter("q", city+","+ state).AddQueryParameter("appid", AppConfigReader.ApiKey);
            var response = client.Execute(request, Method.GET);
            return response.Content;
        }

        public string GetLatestWeatherGeo(double lat, double lon)
        {
            var request = new RestRequest("/weather").AddQueryParameter("lat", lat.ToString()).AddQueryParameter("lon",lon.ToString()).AddQueryParameter("appid", AppConfigReader.ApiKey);
            var response = client.Execute(request, Method.GET);
            return response.Content;
        }


    }
}
