# Weather API Tests

This is a RestSharp project. The following README.md  will talk more about how to use the framework. what to do to maintain the framework and lessons learned.



## How to use this framework & maintain it

This framework uses the RestSharp.NET client to make HTTP REST requests, so is a useful tool to help test an API.

The project has two main folders Tests and Weather Data the later of which contains:

* A response model: to help with testing of the response like an actual object 
* HTTPManager: This is to help with creating the API call where RestSharp is needed and where endpoints are defined.

To add endpoints simply add them to the WeatherCallManager.cs class as long as the response structure is the same as other requests as this must map to the model. 

CurrentWeatherService.cs Class is where we can define what kind of response object we want from what endpoint so it is easier to test.

The Test folder is where we put our tests. one test class for each test endpoint
 

## Lessons learnt

This project had helped me to test APIs using C#. Some of difficulties came when there would be invalid URIs this would take a long time to find the error as the same api query would work in POSTMAN testing. Turns out RestSharp needs and http:// attached to all api query body.

The code currently does not follow good OPP principles as classes are not encapsulated properly this could be rectified next time.

another thing to test for are bad API calls to see if the API fails with the correct messages.
